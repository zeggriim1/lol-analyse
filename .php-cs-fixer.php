<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude([
        '.idea',
        'vendor',
        'public',
        'var'
    ])
;

$config = new PhpCsFixer\Config();
return $config
    ->setUsingCache(false)
    ->setCacheFile(__DIR__ . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'php-cs-fixer.cache')
    ->setRules([
        '@PSR12' => true,
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'strict_param' => true,
        'phpdoc_order' => false,
        'increment_style' => ['style' => 'post'],
        'concat_space' => ['spacing' => 'one'],
        'single_line_empty_body' => true,
        'single_line_throw' => false,
    ])
    ->setFinder($finder)
;

