<?php

namespace App\Repository;

use App\Entity\DataDragon\Champion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Champion>
 *
 * @method Champion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Champion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Champion[]    findAll()
 * @method Champion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChampionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Champion::class);
    }

    public function findChampionByIdAndVersion(int $idChampion, int $idVersion): ?Champion
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.versions', 'versions')
            ->where('c.id = :idChampion')
            ->andWhere('versions.id = :idVersion')
            ->setParameter('idChampion', $idChampion)
            ->setParameter('idVersion', $idVersion)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
