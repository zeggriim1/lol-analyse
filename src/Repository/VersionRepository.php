<?php

namespace App\Repository;

use App\Entity\DataDragon\Version;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Version>
 *
 * @method Version|null find($id, $lockMode = null, $lockVersion = null)
 * @method Version|null findOneBy(array $criteria, array $orderBy = null)
 * @method Version[]    findAll()
 * @method Version[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Version::class);
    }

    /**
     * @return Version[]
     */
    public function findAllValidVersions(?int $maxResult = null): array
    {
        return $this->createQueryBuilder('v')
            ->where('v.version  NOT LIKE :prefix')
            ->andWhere('v.version > :minVersion')
            ->setParameter('prefix', 'lolpatch_%')
            ->setParameter('minVersion', '0')
            ->setMaxResults($maxResult)
            ->getQuery()
            ->getResult()
        ;
    }
}
