<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

class SynchronizeLeagueType extends AbstractType
{
    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tier', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'all' => null,
                    Tier::MASTER->value => Tier::MASTER,
                    Tier::GRANDMASTER->value => Tier::GRANDMASTER,
                    Tier::CHALLENGER->value => Tier::CHALLENGER,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
