<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\InfoChampion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ChampionInfoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        /** @var Champion $championJinx */
        $championJinx = $this->getReference(ChampionFixtures::JINX, Champion::class);

        /** @var Champion $championBard */
        $championBard = $this->getReference(ChampionFixtures::BARD, Champion::class);

        $championJinx->setInfoChampion($this->createInfoChampion(
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10)
        ));
        $manager->persist($championJinx);

        $championBard->setInfoChampion($this->createInfoChampion(
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10),
            $faker->numberBetween(1, 10)
        ));
        $manager->persist($championBard);

        $manager->flush();
    }

    private function createInfoChampion(
        int $attack,
        int $defense,
        int $difficulty,
        int $magic
    ): InfoChampion {
        return (new InfoChampion())
            ->setAttack($attack)
            ->setDefense($defense)
            ->setDifficulty($difficulty)
            ->setMagic($magic)
        ;
    }

    public function getDependencies(): array
    {
        return [ChampionFixtures::class];
    }
}
