<?php

namespace App\DataFixtures;

use App\DataFixtures\Factory\ChampionFactory;
use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\Version;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ChampionFixtures extends Fixture implements DependentFixtureInterface
{
    public const BARD = 'bard';
    public const JINX = 'jinx';
    public const TRYNDAMERE = 'tryndamere';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $version14141 = $this->getReference(VersionFixtures::VERSION_14_14_1, Version::class);
        $version14131 = $this->getReference(VersionFixtures::VERSION_14_13_1, Version::class);

        $champion = $this->createChampion(
            self::TRYNDAMERE,
            (string) $faker->randomNumber(3),
            '1',
            sprintf('Title de %s', self::TRYNDAMERE),
            [$version14141]
        );
        $manager->persist($champion);
        $this->addReference(self::TRYNDAMERE, $champion);

        $champion = $this->createChampion(
            self::BARD,
            (string) $faker->randomNumber(3),
            '2',
            sprintf('Title de %s', self::BARD),
            [$version14131]
        );
        $manager->persist($champion);
        $this->addReference(self::BARD, $champion);

        $champion = $this->createChampion(
            self::JINX,
            (string) $faker->randomNumber(3),
            '3',
            sprintf('Title de %s', self::JINX),
            [$version14131, $version14141]
        );
        $manager->persist($champion);
        $this->addReference(self::JINX, $champion);

        $manager->flush();
    }

    /**
     * @param Version[] $versions
     */
    private function createChampion(
        string $name,
        string $idLol,
        string $key,
        string $title,
        array $versions = []
    ): Champion {
        $champion = ChampionFactory::newChampion(
            $name,
            $idLol,
            $key,
            $title
        );

        $champion->setLore('Lore de tryndamère');
        $champion->setBlurb('Blurb de tryndamère');
        $champion->setVersions($versions);

        return $champion;
    }

    public function getDependencies(): array
    {
        return [VersionFixtures::class];
    }
}
