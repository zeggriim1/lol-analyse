<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Factory\ImageFactory;
use App\Entity\DataDragon\Champion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ChampionImageFixtures extends Fixture implements DependentFixtureInterface
{
    public const JINX_IMAGE_ONE = 'jinx_one';

    public function load(ObjectManager $manager): void
    {
        $jinxChampion = $this->getReference(ChampionFixtures::JINX, Champion::class);
        $championImage = ImageFactory::newChampionImage(
            'Jinx.png',
            'sprite.png',
            'champion',
            144,
            96,
            48,
            48
        );

        $championImage->addEntity($jinxChampion);

        $manager->persist($championImage);
        $this->addReference(self::JINX_IMAGE_ONE, $championImage);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [ChampionFixtures::class];
    }
}
