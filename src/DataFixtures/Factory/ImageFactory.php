<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\ChampionImage;
use App\Entity\SpellImage;

class ImageFactory
{
    public static function newChampionImage(
        string $full = 'full.png',
        string $sprite = 'sprite.png',
        string $group = 'champion',
        int $x = 60,
        int $y = 60,
        int $h = 50,
        int $w = 50
    ): ChampionImage {
        $championImage = new ChampionImage();
        $championImage->setFull($full)
            ->setSprite($sprite)
            ->setGroup($group)
            ->setX($x)
            ->setY($y)
            ->setH($h)
            ->setW($w)
        ;

        return $championImage;
    }

    public static function newSpellImage(
        string $full = 'full.png',
        string $sprite = 'sprite.png',
        string $group = 'spell',
        int $x = 60,
        int $y = 60,
        int $h = 50,
        int $w = 50
    ): SpellImage {
        $championSpell = new SpellImage();
        $championSpell->setFull($full)
            ->setSprite($sprite)
            ->setGroup($group)
            ->setX($x)
            ->setY($y)
            ->setH($h)
            ->setW($w)
        ;

        return $championSpell;
    }
}
