<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\Skin;
use App\Entity\DataDragon\Version;

class SkinFactory
{
    /**
     * @param Version[] $versions
     */
    public static function newSkin(
        string $name,
        string $idLol,
        int $num,
        bool $chromas,
        ?Champion $champion = null,
        array $versions = []
    ): Skin {
        $skin = new Skin();
        $skin->setName($name);
        $skin->setIdLol($idLol);
        $skin->setNum($num);
        $skin->setChromas($chromas);
        $skin->setChampion($champion);
        $skin->setVersions($versions);

        return $skin;
    }
}
