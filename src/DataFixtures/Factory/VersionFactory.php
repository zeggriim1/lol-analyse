<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\DataDragon\Version;

class VersionFactory
{
    public static function newVersion(string $name): Version
    {
        $version = new Version();
        $version->setVersion($name);

        return $version;
    }
}
