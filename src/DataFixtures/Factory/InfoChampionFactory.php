<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\DataDragon\InfoChampion;

class InfoChampionFactory
{
    public static function newInfoChampion(
        int $attack,
        int $defense,
        int $magic,
        int $difficulty
    ): InfoChampion {
        $infoChampion = new InfoChampion();
        $infoChampion->setAttack($attack);
        $infoChampion->setDefense($defense);
        $infoChampion->setMagic($magic);
        $infoChampion->setDifficulty($difficulty);

        return $infoChampion;
    }
}
