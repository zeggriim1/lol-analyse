<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\InfoChampion;
use App\Entity\DataDragon\Skin;
use App\Entity\DataDragon\Version;

class ChampionFactory
{
    /**
     * @param Version[] $version
     * @param Skin[]    $skins
     */
    public static function newChampion(
        string $name,
        string $idLol,
        string $key,
        ?string $title = null,
        array $version = [],
        array $skins = [],
        ?InfoChampion $infoChampion = null
    ): Champion {
        return (new Champion())
            ->setName($name)
            ->setIdLol($idLol)
            ->setTitle($title)
            ->setKeyLol($key)
            ->setVersions($version)
            ->setSkins($skins)
            ->setInfoChampion($infoChampion)
        ;
    }
}
