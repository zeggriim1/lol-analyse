<?php

declare(strict_types=1);

namespace App\DataFixtures\Factory;

use App\Entity\DataDragon\Version;
use App\Entity\League;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

class LeagueFactory
{
    public static function newLeague(
        string $name,
        string $leagueIdLol,
        Queue $queue,
        Tier $tier,
        Version $version
    ): League {
        return (new League())
            ->setName($name)
            ->setLeagueIdLol($leagueIdLol)
            ->setQueue($queue)
            ->setTier($tier)
            ->setVersion($version)
        ;
    }
}
