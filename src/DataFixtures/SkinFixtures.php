<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Factory\SkinFactory;
use App\Entity\DataDragon\Champion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SkinFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        /** @var Champion $championJinx */
        $championJinx = $this->getReference(ChampionFixtures::JINX, Champion::class);

        /** @var Champion $championTryndamere */
        $championTryndamere = $this->getReference(ChampionFixtures::TRYNDAMERE, Champion::class);

        $jinxSkin1 = SkinFactory::newSkin(
            'default',
            (string) $faker->unique()->randomNumber(5, true),
            $faker->randomNumber(4, true),
            $faker->boolean(),
            $championJinx,
            $championJinx->getVersions()->toArray()
        );
        $manager->persist($jinxSkin1);

        $jinxSkin2 = SkinFactory::newSkin(
            'Donna Jinx',
            (string) $faker->unique()->randomNumber(5, true),
            $faker->randomNumber(4, true),
            $faker->boolean(),
            $championJinx,
            $championJinx->getVersions()->toArray()
        );

        $manager->persist($jinxSkin2);

        $tryndamereSkin1 = SkinFactory::newSkin(
            'default',
            (string) $faker->unique()->randomNumber(5, true),
            $faker->randomNumber(4, true),
            $faker->boolean(),
            $championTryndamere,
            $championTryndamere->getVersions()->toArray()
        );
        $manager->persist($tryndamereSkin1);

        $tryndamereSkin2 = SkinFactory::newSkin(
            'Tryndamere viking',
            (string) $faker->unique()->randomNumber(5, true),
            $faker->randomNumber(4, true),
            $faker->boolean(),
            $championTryndamere,
            $championTryndamere->getVersions()->toArray()
        );
        $manager->persist($tryndamereSkin2);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [ChampionFixtures::class];
    }
}
