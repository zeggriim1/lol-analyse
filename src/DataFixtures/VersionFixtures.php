<?php

namespace App\DataFixtures;

use App\Entity\DataDragon\Version;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VersionFixtures extends Fixture
{
    public const VERSION_14_14_1 = '14.14.1';
    public const VERSION_14_13_1 = '14.13.1';
    public const VERSION_14_12_1 = '14.12.1';
    public const VERSION_13_1_1 = '13.1.1';
    public const VERSION_13_3_1 = '13.3.1';
    public const VERSION_13_4_1 = '13.4.1';
    public const VERSIONS = [
        self::VERSION_14_14_1,
        self::VERSION_14_13_1,
        self::VERSION_14_12_1,
        self::VERSION_13_1_1,
        self::VERSION_13_3_1,
        self::VERSION_13_4_1,
    ];

    public function load(ObjectManager $manager): void
    {
        $first = true;
        foreach (self::VERSIONS as $version) {
            $versionEntity = (new Version())
                ->setVersion($version)
            ;

            if ($first) {
                $versionEntity->setActive(true);
                $first = false;
            }

            $manager->persist($versionEntity);
            $this->addReference($version, $versionEntity);
        }

        $manager->flush();
    }
}
