<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

class KernelRequestSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly Environment $twig, private readonly bool $isMaintenanceMode = false) {}

    public function onKernelRequest(RequestEvent $event): void
    {
        $hasAdmin = str_starts_with($event->getRequest()->getRequestUri(), '/admin');

        if ($this->isMaintenanceMode && !$hasAdmin) {
            $event->setResponse(new Response(
                $this->twig->render('in_progress.html.twig'),
                Response::HTTP_SERVICE_UNAVAILABLE,
            ));
            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
