<?php

namespace App\MessageHandler;

use App\Message\SynchronizeLanguage;
use App\Services\Synchronizer\LanguageSynchronizer;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class SynchronizeLanguageHandler
{
    public function __construct(private LanguageSynchronizer $languageSynchronizer) {}

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __invoke(SynchronizeLanguage $message): void
    {
        $this->languageSynchronizer->synchronize();
    }
}
