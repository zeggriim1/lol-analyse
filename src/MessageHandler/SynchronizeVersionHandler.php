<?php

namespace App\MessageHandler;

use App\Message\SynchronizeVersion;
use App\Services\Synchronizer\VersionSynchronizer;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class SynchronizeVersionHandler
{
    public function __construct(private VersionSynchronizer $versionSynchronizer) {}

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __invoke(SynchronizeVersion $message): void
    {
        $this->versionSynchronizer->synchronize();
    }
}
