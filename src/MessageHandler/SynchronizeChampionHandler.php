<?php

namespace App\MessageHandler;

use App\Message\SynchronizeChampion;
use App\Repository\VersionRepository;
use App\Services\Synchronizer\ChampionSynchronizer;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;

#[AsMessageHandler]
final readonly class SynchronizeChampionHandler
{
    public function __construct(
        private ChampionSynchronizer $championSynchronizer,
        private VersionRepository $versionRepository
    ) {}

    public function __invoke(SynchronizeChampion $message): void
    {
        $version = $this->versionRepository->find($message->idVersion);

        if (null === $version) {
            throw new UnrecoverableMessageHandlingException(sprintf('not found from import %d', $message->idVersion));
        }

        $this->championSynchronizer->synchronize($version);
    }
}
