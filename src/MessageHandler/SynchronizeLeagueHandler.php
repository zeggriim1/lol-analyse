<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\SynchronizeLeague;
use App\Repository\VersionRepository;
use App\Services\Synchronizer\LeagueSynchronizer;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

#[AsMessageHandler]
class SynchronizeLeagueHandler
{
    public function __construct(
        private readonly VersionRepository $versionRepository,
        private readonly LeagueSynchronizer $leagueSynchronizer
    ) {}

    public function __invoke(SynchronizeLeague $message): void
    {
        $version = $this->versionRepository->find($message->idVersion);
        $queue = Queue::tryFrom($message->queue);
        $tier = Tier::tryFrom($message->tier);

        if (null === $version) {
            throw new UnrecoverableMessageHandlingException(sprintf('not found from import %d', $message->idVersion));
        }

        $this->leagueSynchronizer->synchronize($version, $queue, $tier);
    }
}
