<?php

namespace App\MessageHandler;

use App\Message\SynchronizeSkin;
use App\Repository\ChampionRepository;
use App\Repository\VersionRepository;
use App\Services\Synchronizer\SkinSynchronizer;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;

#[AsMessageHandler]
final readonly class SynchronizeSkinHandler
{
    public function __construct(
        private ChampionRepository $championRepository,
        private VersionRepository $versionRepository,
        private SkinSynchronizer $skinSynchronizer
    ) {}

    public function __invoke(SynchronizeSkin $message): void
    {
        $version = $this->versionRepository->find($message->idVersion);
        $champion = $this->championRepository->find($message->idChampion);

        if (null === $version || null === $champion) {
            throw new UnrecoverableMessageHandlingException(
                sprintf(
                    'not found from import (Version: %d, Champion: %d)',
                    $message->idVersion,
                    $message->idChampion
                )
            );
        }

        $this->skinSynchronizer->synchronize($champion, $version);
    }
}
