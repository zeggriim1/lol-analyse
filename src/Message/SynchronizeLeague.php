<?php

declare(strict_types=1);

namespace App\Message;

class SynchronizeLeague
{
    public function __construct(
        public int $idVersion,
        public string $tier,
        public string $queue
    ) {}
}
