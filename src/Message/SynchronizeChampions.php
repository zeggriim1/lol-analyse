<?php

namespace App\Message;

final class SynchronizeChampions
{
    public function __construct(
        public readonly int $idVersion,
    ) {}
}
