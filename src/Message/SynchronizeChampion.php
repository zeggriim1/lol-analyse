<?php

namespace App\Message;

final readonly class SynchronizeChampion
{
    public function __construct(
        public int $idVersion,
    ) {}
}
