<?php

namespace App\Message;

final readonly class SynchronizeSkin
{
    public function __construct(
        public int $idChampion,
        public int $idVersion
    ) {}
}
