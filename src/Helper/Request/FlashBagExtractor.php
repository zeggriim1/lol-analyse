<?php

declare(strict_types=1);

namespace App\Helper\Request;

use App\Exception\SessionHasNoFlashBagException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class FlashBagExtractor
{
    public static function fromRequest(Request $request): FlashBagInterface
    {
        $session = $request->getSession();

        if (!$session instanceof Session) {
            throw new SessionHasNoFlashBagException($session);
        }

        return $session->getFlashBag();
    }
}
