<?php

declare(strict_types=1);

namespace App\Helper\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

use function Symfony\Component\Translation\t;

class ConfigureCrudHelper
{
    public static function buildConfigureCrud(Crud $crud, string $name): Crud
    {
        return $crud
            ->setPageTitle(
                Crud::PAGE_INDEX,
                t('menu.easy_admin.index', ['name' => ucfirst($name)], 'admin')
            )
            ->setPageTitle(
                Crud::PAGE_NEW,
                t('menu.easy_admin.new', ['name' => $name], 'admin')
            )
            ->setPageTitle(
                Crud::PAGE_EDIT,
                t('menu.easy_admin.edit', ['name' => $name], 'admin')
            )
            ->setPageTitle(
                Crud::PAGE_DETAIL,
                t('menu.easy_admin.detail', ['name' => ucfirst($name)], 'admin')
            )
        ;
    }
}
