<?php

declare(strict_types=1);

namespace App\Helper;

use Symfony\Component\HttpFoundation\Request;

class RequestHelper
{
    public static function redirectionToReferer(Request $request): ?string
    {
        $referer = $request->headers->get('referer');

        if (null === $referer) {
            throw new \RuntimeException('Referer is empty');
        }

        if (false !== $pos = strpos($referer, '?')) {
            $referer = substr($referer, 0, $pos);
        }

        return $referer;
    }
}
