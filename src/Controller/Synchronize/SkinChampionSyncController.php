<?php

declare(strict_types=1);

namespace App\Controller\Synchronize;

use App\Helper\Request\FlashBagExtractor;
use App\Helper\RequestHelper;
use App\Message\SynchronizeSkin;
use App\Repository\ChampionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class SkinChampionSyncController extends AbstractController
{
    public function __construct(private readonly ChampionRepository $championRepository) {}

    #[Route('/admin/skin-champion-sync', name: 'app_skins_champion_sync')]
    public function __invoke(Request $request, MessageBusInterface $bus): RedirectResponse
    {
        $flash = FlashBagExtractor::fromRequest($request);

        $champions = $this->championRepository->findAll();

        if (\count($champions) > 0) {
            foreach ($champions as $champion) {
                foreach ($champion->getVersions() as $version) {
                    $bus->dispatch(new SynchronizeSkin($champion->getId(), $version->getId()));
                }
            }
            $flash->add('success', 'Synchro valide');
        } else {
            $flash->add('info', 'Aucun champion trouvé.');
        }

        $referer = RequestHelper::redirectionToReferer($request);

        return new RedirectResponse($referer, Response::HTTP_SEE_OTHER);
    }
}
