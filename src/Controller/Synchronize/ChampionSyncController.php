<?php

declare(strict_types=1);

namespace App\Controller\Synchronize;

use App\Helper\Request\FlashBagExtractor;
use App\Helper\RequestHelper;
use App\Message\SynchronizeChampion;
use App\Repository\VersionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class ChampionSyncController extends AbstractController
{
    public function __construct(private readonly VersionRepository $versionRepository) {}

    #[Route('/admin/champion-sync', name: 'app_champion_sync')]
    public function __invoke(Request $request, MessageBusInterface $bus): RedirectResponse
    {
        $flash = FlashBagExtractor::fromRequest($request);

        $versions = $this->versionRepository->findAllValidVersions();

        if (\count($versions) > 0) {
            foreach ($versions as $version) {
                $bus->dispatch(new SynchronizeChampion($version->getId()));
            }
        }

        $referer = RequestHelper::redirectionToReferer($request);

        $flash->add('success', 'Synchro valide');

        return new RedirectResponse($referer, Response::HTTP_SEE_OTHER);
    }
}
