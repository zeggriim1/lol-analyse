<?php

declare(strict_types=1);

namespace App\Controller\Synchronize;

use App\Helper\Request\FlashBagExtractor;
use App\Helper\RequestHelper;
use App\Message\SynchronizeLanguage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class LanguageSyncController extends AbstractController
{
    #[Route('/admin/language-sync', name: 'app_language_sync')]
    public function __invoke(Request $request, MessageBusInterface $bus): RedirectResponse
    {
        $flash = FlashBagExtractor::fromRequest($request);

        $bus->dispatch(new SynchronizeLanguage());

        $referer = RequestHelper::redirectionToReferer($request);

        $flash->add('success', 'Synchro valide');

        return new RedirectResponse($referer, Response::HTTP_SEE_OTHER);
    }
}
