<?php

declare(strict_types=1);

namespace App\Controller\Synchronize;

use App\Helper\Request\FlashBagExtractor;
use App\Helper\RequestHelper;
use App\Message\SynchronizeVersion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class VersionSyncController extends AbstractController
{
    #[Route('/admin/version-sync', name: 'app_version_sync')]
    public function __invoke(Request $request, MessageBusInterface $bus): RedirectResponse
    {
        $flash = FlashBagExtractor::fromRequest($request);

        $bus->dispatch(new SynchronizeVersion());

        $referer = RequestHelper::redirectionToReferer($request);

        $flash->add('success', 'Synchro valide');

        return new RedirectResponse($referer, Response::HTTP_SEE_OTHER);
    }
}
