<?php

declare(strict_types=1);

namespace App\Controller\Synchronize;

use App\Form\SynchronizeLeagueType;
use App\Helper\Request\FlashBagExtractor;
use App\Repository\VersionRepository;
use App\Services\LeagueSyncManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LeagueSyncController extends AbstractController
{
    #[Route('/admin/league-sync', name: 'app_league_sync')]
    public function __invoke(
        Request $request,
        VersionRepository $versionRepository,
        LeagueSyncManager $leagueSyncManager
    ): Response {
        $form = $this->createForm(SynchronizeLeagueType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tier = $form->getData()['tier'];

            $version = $versionRepository->findOneBy(['active' => true]);
            $flash = FlashBagExtractor::fromRequest($request);

            if (null !== $version) {
                $leagueSyncManager->dispatchMessage($version, $tier);
                $flash->add('success', 'Synchro valide');
            } else {
                $flash->add('warning', 'Aucun version n\'est actif');
            }
        }

        return $this->render('synchronize/league_sync.html.twig', [
            'form_sync_league' => $form->createView(),
        ]);
    }
}
