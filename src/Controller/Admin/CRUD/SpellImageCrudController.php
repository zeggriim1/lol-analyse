<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\SpellImage;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use function Symfony\Component\Translation\t;

class SpellImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SpellImage::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return ConfigureCrudHelper::buildConfigureCrud($crud, 'image spell');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t('spellImage.id', [], 'admin'))->hideOnForm(),
            TextField::new('full', t('spellImage.full', [], 'admin')),
            TextField::new('sprite', t('spellImage.sprite', [], 'admin')),
            TextField::new('group', t('spellImage.group', [], 'admin')),
            IntegerField::new('x', t('spellImage.x', [], 'admin')),
            IntegerField::new('y', t('spellImage.y', [], 'admin')),
            IntegerField::new('w', t('spellImage.w', [], 'admin')),
            IntegerField::new('h', t('spellImage.h', [], 'admin')),
            AssociationField::new('entities', t('spellImage.entities', [], 'admin')),
            DateTimeField::new('createdAt', t('spellImage.createdAt', [], 'admin')),
            DateTimeField::new('updatedAt', t('spellImage.updatedAt', [], 'admin')),
        ];
    }
}
