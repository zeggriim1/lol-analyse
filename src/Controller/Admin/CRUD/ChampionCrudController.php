<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\DataDragon\Champion;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class ChampionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Champion::class;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'champion.id', domain: 'admin')),
            TextField::new('idLol', t(message: 'champion.idLol', domain: 'admin')),
            TextField::new('name', t(message: 'champion.name', domain: 'admin')),
            TextField::new('keyLol', t(message: 'champion.keyLol', domain: 'admin')),
            TextField::new('title', t(message: 'champion.title', domain: 'admin')),
            TextareaField::new('Lore', t(message: 'champion.lore', domain: 'admin')),
            TextareaField::new('blurb', t(message: 'champion.blurb', domain: 'admin')),
            AssociationField::new('versions', new TranslatableMessage(message: 'champion.version', domain: 'admin')),
            DateTimeField::new('createdAt', t(message: 'champion.createdAt', domain: 'admin'))->hideOnForm(),
            DateTimeField::new('updatedAt', t(message: 'champion.updatedAt', domain: 'admin'))->hideOnForm(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud = ConfigureCrudHelper::buildConfigureCrud($crud, 'champion');

        return $crud->setDefaultSort(['id' => 'ASC']);
    }
}
