<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\DataDragon\InfoChampion;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

use function Symfony\Component\Translation\t;

class InfoChampionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return InfoChampion::class;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'info.id', domain: 'admin')),
            IntegerField::new('attack', t(message: 'info.attack', domain: 'admin')),
            IntegerField::new('defense', t(message: 'info.defense', domain: 'admin')),
            IntegerField::new('magic', t(message: 'info.magic', domain: 'admin')),
            IntegerField::new('difficulty', t(message: 'info.difficulty', domain: 'admin')),
            AssociationField::new('champion', t(message: 'info.champion', domain: 'admin')),
            DateTimeField::new('createdAt', t(message: 'info.createdAt', domain: 'admin'))->hideOnForm(),
            DateTimeField::new('updatedAt', t(message: 'info.updatedAt', domain: 'admin'))->hideOnForm(),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('champion')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud = ConfigureCrudHelper::buildConfigureCrud($crud, 'info champion');

        return $crud->setDefaultSort(['id' => 'ASC']);
    }
}
