<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\League;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

use function Symfony\Component\Translation\t;

class LeagueCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return League::class;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'league.id', domain: 'admin')),
            TextField::new('name', t(message: 'league.name', domain: 'admin')),
            TextField::new('tierString', t(message: 'league.tier', domain: 'admin'))->setLabel('Tier'),
            TextField::new('queueString', t(message: 'league.queue', domain: 'admin'))->setLabel('Queue'),
            AssociationField::new('version', t(message: 'league.version', domain: 'admin')),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id' => 'ASC']);
    }
}
