<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\DataDragon\Language;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

use function Symfony\Component\Translation\t;

class LanguageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Language::class;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'language.id', domain: 'admin')),
            TextField::new('code', t(message: 'language.code', domain: 'admin')),
            DateTimeField::new('createdAt', t(message: 'language.createdAt', domain: 'admin'))->hideOnForm(),
            DateTimeField::new('updatedAt', t(message: 'language.updatedAt', domain: 'admin'))->hideOnForm(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id' => 'DESC']);
    }
}
