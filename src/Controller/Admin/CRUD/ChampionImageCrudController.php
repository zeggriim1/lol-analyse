<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\ChampionImage;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use function Symfony\Component\Translation\t;

class ChampionImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ChampionImage::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return ConfigureCrudHelper::buildConfigureCrud($crud, 'image champion');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t('championImage.id', [], 'admin'))->hideOnForm(),
            TextField::new('full', t('championImage.full', [], 'admin')),
            TextField::new('sprite', t('championImage.sprite', [], 'admin')),
            TextField::new('group', t('championImage.group', [], 'admin')),
            IntegerField::new('x', t('championImage.x', [], 'admin')),
            IntegerField::new('y', t('championImage.y', [], 'admin')),
            IntegerField::new('w', t('championImage.w', [], 'admin')),
            IntegerField::new('h', t('championImage.h', [], 'admin')),
            AssociationField::new('entities', t('championImage.entities', [], 'admin')),
            DateTimeField::new('createdAt', t('spellImage.createdAt', [], 'admin')),
            DateTimeField::new('updatedAt', t('spellImage.updatedAt', [], 'admin')),
        ];
    }
}
