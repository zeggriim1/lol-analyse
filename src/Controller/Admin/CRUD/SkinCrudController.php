<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\DataDragon\Skin;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

use function Symfony\Component\Translation\t;

class SkinCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Skin::class;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'skin.id', domain: 'admin')),
            TextField::new('name', t(message: 'skin.name', domain: 'admin')),
            IdField::new('idLol', t(message: 'skin.idlol', domain: 'admin')),
            IntegerField::new('num', t(message: 'skin.num', domain: 'admin')),
            BooleanField::new('chromas', t(message: 'skin.chromas', domain: 'admin')),
            AssociationField::new('champion', t(message: 'skin.champion', domain: 'admin')),
            AssociationField::new('versions', t(message: 'skin.versions', domain: 'admin')),
            DateTimeField::new('createdAt', t(message: 'skin.createdAt', domain: 'admin')),
            DateTimeField::new('updatedAt', t(message: 'skin.updatedAt', domain: 'admin')),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud = ConfigureCrudHelper::buildConfigureCrud($crud, 'skin');

        return $crud->setDefaultSort(['id' => 'ASC']);
    }
}
