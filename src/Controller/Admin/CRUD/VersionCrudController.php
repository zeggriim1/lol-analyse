<?php

namespace App\Controller\Admin\CRUD;

use App\Entity\DataDragon\Version;
use App\Helper\Admin\ConfigureCrudHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

use function Symfony\Component\Translation\t;

class VersionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Version::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud = ConfigureCrudHelper::buildConfigureCrud($crud, 'version');
        $crud->setDefaultSort(['id' => 'ASC']);

        return $crud;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', t(message: 'version.id', domain: 'admin')),
            TextField::new('version', t(message: 'version.version', domain: 'admin')),
            BooleanField::new('active', t(message: 'version.active', domain: 'admin')),
            DateTimeField::new('createdAt', t(message: 'version.createdAt', domain: 'admin'))->hideOnForm(),
            DateTimeField::new('updatedAt', t(message: 'version.updatedAt', domain: 'admin'))->hideOnForm(),
        ];
    }
}
