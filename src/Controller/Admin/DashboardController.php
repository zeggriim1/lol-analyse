<?php

namespace App\Controller\Admin;

use App\Entity\ChampionImage;
use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\InfoChampion;
use App\Entity\DataDragon\Language;
use App\Entity\DataDragon\Skin;
use App\Entity\DataDragon\Version;
use App\Entity\League;
use App\Entity\SpellImage;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->translator->trans('menu.title.menu', domain: 'admin'))
            ->setTranslationDomain('admin')
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('menu.title.dashboard', 'fa fa-home');
        yield MenuItem::subMenu('menu.title.champions', 'fas fa-list')->setSubItems([
            MenuItem::linkToCrud('menu.title.champion', 'fas fa-list', Champion::class),
            MenuItem::linkToCrud('menu.title.info', 'fas fa-list', InfoChampion::class),
            MenuItem::linkToCrud('menu.title.skin', 'fas fa-list', Skin::class),
        ]);
        yield MenuItem::subMenu('menu.title.initialisation', 'fas fa-list')->setSubItems([
            MenuItem::linkToCrud('menu.title.languages', 'fas fa-language', Language::class),
            MenuItem::linkToCrud('menu.title.version', 'fas fa-list', Version::class),
            MenuItem::linkToCrud('menu.title.league', 'fas fa-list', League::class),
        ]);
        yield MenuItem::subMenu('menu.title.data dragon', 'fas fa-list')->setSubItems([
            MenuItem::linkToRoute('menu.title.sync league', 'fas fa-list', 'app_league_sync'),
        ]);
        yield MenuItem::subMenu('menu.title.image', 'fas fa-image')->setSubItems([
            MenuItem::linkToCrud('menu.title.image_champion', 'fas fa-list', ChampionImage::class),
            MenuItem::linkToCrud('menu.title.image_spell', 'fas fa-list', SpellImage::class),
        ]);
    }
}
