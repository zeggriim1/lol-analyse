<?php

namespace App\Entity\Interface;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;

/**
 * @template Entity of object
 */
interface ImageInheritanceInterface
{
    /**
     * @return Collection<int, Entity>
     */
    public function getEntities(): Collection;

    /**
     * @param Collection<int, Entity> $entities
     */
    public function setEntities(Collection $entities): void;

    /**
     * @param Entity $entity
     */
    public function addEntity(object $entity): void;

    /**
     * @param Entity $entity
     */
    public function removeEntity(object $entity): void;
}
