<?php

namespace App\Entity;

use App\Entity\DataDragon\Version;
use App\Repository\LeagueRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

#[ORM\Entity(repositoryClass: LeagueRepository::class)]
class League
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, enumType: Tier::class)]
    private ?Tier $tier = null;

    #[ORM\Column(length: 255)]
    private ?string $leagueIdLol = null;

    #[ORM\Column(length: 40, enumType: Queue::class)]
    private ?Queue $queue = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'leagues')]
    private ?Version $version = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTier(): ?Tier
    {
        return $this->tier;
    }

    public function getTierString(): ?string
    {
        return $this->tier->value;
    }

    public function setTier(Tier $tier): static
    {
        $this->tier = $tier;

        return $this;
    }

    public function getLeagueIdLol(): ?string
    {
        return $this->leagueIdLol;
    }

    public function setLeagueIdLol(string $leagueIdLol): static
    {
        $this->leagueIdLol = $leagueIdLol;

        return $this;
    }

    public function getQueue(): ?Queue
    {
        return $this->queue;
    }

    public function getQueueString(): ?string
    {
        return $this->queue->value;
    }

    public function setQueue(Queue $queue): static
    {
        $this->queue = $queue;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): static
    {
        $this->version = $version;

        return $this;
    }
}
