<?php

namespace App\Entity;

use App\Entity\Traits\IdEntityTrait;
use App\Repository\RangeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RangeRepository::class)]
#[ORM\Table(name: '`spell_range`')]
class Range
{
    use IdEntityTrait;

    #[ORM\Column]
    private ?int $value0 = null;

    #[ORM\Column]
    private ?int $value1 = null;

    #[ORM\Column]
    private ?int $value2 = null;

    #[ORM\Column]
    private ?int $value3 = null;

    #[ORM\Column]
    private ?int $value4 = null;

    public function getValue0(): ?int
    {
        return $this->value0;
    }

    public function setValue0(int $value0): static
    {
        $this->value0 = $value0;

        return $this;
    }

    public function getValue1(): ?int
    {
        return $this->value1;
    }

    public function setValue1(int $value1): static
    {
        $this->value1 = $value1;

        return $this;
    }

    public function getValue2(): ?int
    {
        return $this->value2;
    }

    public function setValue2(int $value2): static
    {
        $this->value2 = $value2;

        return $this;
    }

    public function getValue3(): ?int
    {
        return $this->value3;
    }

    public function setValue3(int $value3): static
    {
        $this->value3 = $value3;

        return $this;
    }

    public function getValue4(): ?int
    {
        return $this->value4;
    }

    public function setValue4(int $value4): static
    {
        $this->value4 = $value4;

        return $this;
    }
}
