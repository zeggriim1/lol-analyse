<?php

namespace App\Entity;

use App\Entity\Traits\IdEntityTrait;
use App\Repository\LeveltipRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LeveltipRepository::class)]
#[ORM\Table(name: 'spell_leveltip')]
class Leveltip
{
    use IdEntityTrait;

    #[ORM\Column(type: Types::STRING, length: 150)]
    private ?string $label0 = null;

    #[ORM\Column(type: Types::STRING, length: 150)]
    private ?string $label1 = null;

    #[ORM\Column(type: Types::STRING, length: 150)]
    private ?string $label2 = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $effect0 = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $effect1 = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $effect2 = null;

    public function getLabel0(): ?string
    {
        return $this->label0;
    }

    public function setLabel0(string $label0): static
    {
        $this->label0 = $label0;

        return $this;
    }

    public function getLabel1(): ?string
    {
        return $this->label1;
    }

    public function setLabel1(string $label1): static
    {
        $this->label1 = $label1;

        return $this;
    }

    public function getLabel2(): ?string
    {
        return $this->label2;
    }

    public function setLabel2(string $label2): static
    {
        $this->label2 = $label2;

        return $this;
    }

    public function getEffect0(): ?string
    {
        return $this->effect0;
    }

    public function setEffect0(string $effect0): static
    {
        $this->effect0 = $effect0;

        return $this;
    }

    public function getEffect1(): ?string
    {
        return $this->effect1;
    }

    public function setEffect1(string $effect1): static
    {
        $this->effect1 = $effect1;

        return $this;
    }

    public function getEffect2(): ?string
    {
        return $this->effect2;
    }

    public function setEffect2(string $effect2): static
    {
        $this->effect2 = $effect2;

        return $this;
    }
}
