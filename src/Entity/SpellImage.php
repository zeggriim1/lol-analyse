<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\DataDragon\Image;
use App\Entity\DataDragon\Spell;
use App\Entity\Interface\ImageInheritanceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @implements ImageInheritanceInterface<Spell>
 */
#[ORM\Entity]
class SpellImage extends Image implements ImageInheritanceInterface
{
    /**
     * @var Collection<int, Spell>
     */
    #[ORM\ManyToMany(targetEntity: Spell::class, inversedBy: 'images')]
    #[ORM\JoinTable(name: 'spell_images')]
    private Collection $entities;

    public function __construct()
    {
        $this->entities = new ArrayCollection();
    }

    /**
     * @return Collection<int, Spell>
     */
    public function getEntities(): Collection
    {
        return $this->entities;
    }

    /**
     * @param Collection<int, Spell> $entities
     */
    public function setEntities(Collection $entities): void
    {
        $this->entities = $entities;
    }

    public function addEntity(object $entity): void
    {
        if (!$this->entities->contains($entity)) {
            $this->entities->add($entity);
        }
    }

    public function removeEntity(object $entity): void
    {
        if ($this->entities->contains($entity)) {
            $this->entities->removeElement($entity);
        }
    }
}
