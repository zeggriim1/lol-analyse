<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\Image;
use App\Entity\Interface\ImageInheritanceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @implements ImageInheritanceInterface<Champion>
 */
#[ORM\Entity]
class ChampionImage extends Image implements ImageInheritanceInterface
{
    /**
     * @var Collection<int, Champion>
     */
    #[ORM\ManyToMany(targetEntity: Champion::class, inversedBy: 'images')]
    #[ORM\JoinTable(name: 'champion_images')]
    private Collection $entities;

    public function __construct()
    {
        $this->entities = new ArrayCollection();
    }

    /**
     * @return Collection<int, Champion>
     */
    public function getEntities(): Collection
    {
        return $this->entities;
    }

    /**
     * @param Collection<int, Champion> $entities
     */
    public function setEntities(Collection $entities): void
    {
        $this->entities = $entities;
    }

    public function addEntity($entity): void
    {
        if (!$this->entities->contains($entity)) {
            $this->entities->add($entity);
        }
    }

    public function removeEntity($entity): void
    {
        if ($this->entities->contains($entity)) {
            $this->entities->removeElement($entity);
        }
    }
}
