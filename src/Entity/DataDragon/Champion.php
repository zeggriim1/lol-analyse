<?php

declare(strict_types=1);

namespace App\Entity\DataDragon;

use App\Entity\ChampionImage;
use App\Entity\Traits\IdEntityTrait;
use App\Repository\ChampionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: ChampionRepository::class)]
class Champion
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(name: 'id_lol', type: Types::STRING, nullable: false)]
    private ?string $idLol;

    #[ORM\Column(name: 'name', type: Types::STRING, nullable: false)]
    private ?string $name;

    #[ORM\Column(name: 'key_lol', type: Types::STRING, nullable: false)]
    private ?string $keyLol;

    #[ORM\Column(name: 'title', type: Types::STRING, nullable: true)]
    private ?string $title;

    #[ORM\Column(name: 'blurb', type: Types::TEXT, nullable: true)]
    private ?string $blurb = null;

    #[ORM\Column(name: 'lore', type: Types::TEXT, nullable: true)]
    private ?string $lore = null;

    /**
     * @var Collection<int, Version>
     */
    #[ORM\ManyToMany(targetEntity: Version::class, inversedBy: 'champions')]
    private Collection $versions;

    /**
     * @var Collection<int, Skin>
     */
    #[ORM\OneToMany(mappedBy: 'Champion', targetEntity: Skin::class)]
    private Collection $skins;

    #[ORM\OneToOne(mappedBy: 'champion', cascade: ['persist', 'remove'])]
    private ?InfoChampion $infoChampion = null;

    /**
     * @var Collection<int, ChampionImage>
     */
    #[ORM\ManyToMany(targetEntity: ChampionImage::class, mappedBy: 'entities', cascade: ['persist', 'remove'])]
    private Collection $images;

    /**
     * @var Collection<int, Spell>
     */
    #[ORM\OneToMany(mappedBy: 'champion', targetEntity: Spell::class)]
    private Collection $spells;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->versions = new ArrayCollection();
        $this->skins = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->spells = new ArrayCollection();
    }

    public function getIdLol(): ?string
    {
        return $this->idLol;
    }

    public function setIdLol(?string $idLol): static
    {
        $this->idLol = $idLol;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getKeyLol(): ?string
    {
        return $this->keyLol;
    }

    public function setKeyLol(?string $keyLol): static
    {
        $this->keyLol = $keyLol;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getBlurb(): ?string
    {
        return $this->blurb;
    }

    public function setBlurb(?string $blurb): static
    {
        $this->blurb = $blurb;

        return $this;
    }

    public function getLore(): ?string
    {
        return $this->lore;
    }

    public function setLore(?string $lore): void
    {
        $this->lore = $lore;
    }

    /**
     * @return Collection<int, Version>
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    /**
     * @param Version[] $versions
     */
    public function setVersions(array $versions): static
    {
        foreach ($versions as $version) {
            $this->addVersion($version);
        }

        return $this;
    }

    public function addVersion(Version $version): static
    {
        if (!$this->versions->contains($version)) {
            $this->versions->add($version);
        }

        return $this;
    }

    public function removeVersion(Version $version): static
    {
        $this->versions->removeElement($version);

        return $this;
    }

    /**
     * @return Collection<int, Skin>
     */
    public function getSkins(): Collection
    {
        return $this->skins;
    }

    public function addSkin(Skin $skin): static
    {
        if (!$this->skins->contains($skin)) {
            $this->skins->add($skin);
            $skin->setChampion($this);
        }

        return $this;
    }

    public function removeSkin(Skin $skin): static
    {
        if ($this->skins->removeElement($skin)) {
            if ($skin->getChampion() === $this) {
                $skin->setChampion(null);
            }
        }

        return $this;
    }

    /**
     * @param Skin[] $skins
     */
    public function setSkins(array $skins): static
    {
        foreach ($skins as $skin) {
            $this->addSkin($skin);
        }

        return $this;
    }

    public function getInfoChampion(): ?InfoChampion
    {
        return $this->infoChampion;
    }

    public function setInfoChampion(?InfoChampion $infoChampion): static
    {
        // unset the owning side of the relation if necessary
        if (null === $infoChampion && null !== $this->infoChampion) {
            $this->infoChampion->setChampion(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $infoChampion && $infoChampion->getChampion() !== $this) {
            $infoChampion->setChampion($this);
        }

        $this->infoChampion = $infoChampion;

        return $this;
    }

    /**
     * @return Collection<int, ChampionImage>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param Collection<int, ChampionImage> $images
     */
    public function setImages(Collection $images): void
    {
        $this->images = $images;
    }

    public function addImage(?ChampionImage $image): void
    {
        if (null === $image) {
            return;
        }

        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }

    public function removeImage(ChampionImage $image): void
    {
        if ($this->getImages()->contains($image)) {
            $this->images->removeElement($image);
        }
    }

    /**
     * @return Collection<int, Spell>
     */
    public function getSpells(): Collection
    {
        return $this->spells;
    }

    public function addSpell(Spell $spell): static
    {
        if (!$this->spells->contains($spell)) {
            $this->spells->add($spell);
            $spell->setChampion($this);
        }

        return $this;
    }

    public function removeSpell(Spell $spell): static
    {
        if ($this->spells->removeElement($spell)) {
            // set the owning side to null (unless already changed)
            if ($spell->getChampion() === $this) {
                $spell->setChampion(null);
            }
        }

        return $this;
    }
}
