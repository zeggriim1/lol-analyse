<?php

declare(strict_types=1);

namespace App\Entity\DataDragon;

use App\Entity\ChampionImage;
use App\Entity\SpellImage;
use App\Entity\Traits\IdEntityTrait;
use App\Repository\DataDragon\ImageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: ImageRepository::class)]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap(['spell_image' => SpellImage::class, 'champion_image' => ChampionImage::class])]
class Image
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(type: Types::STRING, length: 100, nullable: false)]
    private string $full;
    #[ORM\Column(type: Types::STRING, length: 100, nullable: false)]
    private string $sprite;
    #[ORM\Column(name: 'category', type: Types::STRING, length: 100, nullable: false)]
    private string $group;
    #[ORM\Column(type: Types::INTEGER, length: 10, nullable: false)]
    private int $x;
    #[ORM\Column(type: Types::INTEGER, length: 10, nullable: false)]
    private int $y;
    #[ORM\Column(type: Types::INTEGER, length: 10, nullable: false)]
    private int $w;
    #[ORM\Column(type: Types::INTEGER, length: 10, nullable: false)]
    private int $h;

    public function getFull(): string
    {
        return $this->full;
    }

    public function setFull(string $full): self
    {
        $this->full = $full;

        return $this;
    }

    public function getSprite(): string
    {
        return $this->sprite;
    }

    public function setSprite(string $sprite): self
    {
        $this->sprite = $sprite;

        return $this;
    }

    public function getGroup(): string
    {
        return $this->group;
    }

    public function setGroup(string $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getW(): int
    {
        return $this->w;
    }

    public function setW(int $w): self
    {
        $this->w = $w;

        return $this;
    }

    public function getH(): int
    {
        return $this->h;
    }

    public function setH(int $h): self
    {
        $this->h = $h;

        return $this;
    }
}
