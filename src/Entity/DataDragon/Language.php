<?php

namespace App\Entity\DataDragon;

use App\Entity\Traits\IdEntityTrait;
use App\Repository\LanguageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: LanguageRepository::class)]
#[UniqueEntity('code')]
class Language
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private ?string $code = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
