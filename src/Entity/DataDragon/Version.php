<?php

namespace App\Entity\DataDragon;

use App\Entity\League;
use App\Entity\Traits\IdEntityTrait;
use App\Repository\VersionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: VersionRepository::class)]
#[UniqueEntity('version')]
class Version
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(type: Types::STRING, length: 20, unique: true, nullable: false)]
    #[Assert\Length(max: 20)]
    private ?string $version = null;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $active = false;

    /**
     * @var Collection<int, Champion>
     */
    #[ORM\ManyToMany(targetEntity: Champion::class, mappedBy: 'versions')]
    private Collection $champions;

    /**
     * @var Collection<int, League>
     */
    #[ORM\OneToMany(mappedBy: 'version', targetEntity: League::class)]
    private Collection $leagues;

    /**
     * @var Collection<int, Skin>
     */
    #[ORM\ManyToMany(targetEntity: Champion::class, mappedBy: 'versions')]
    private Collection $skins;

    public function __construct()
    {
        $this->champions = new ArrayCollection();
        $this->leagues = new ArrayCollection();
        $this->skins = new ArrayCollection();
    }

    public function __toString(): string
    {
        return sprintf('%s', $this->getVersion());
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection<int, Champion>
     */
    public function getChampions(): Collection
    {
        return $this->champions;
    }

    public function addChampion(Champion $champion): static
    {
        if (!$this->champions->contains($champion)) {
            $this->champions->add($champion);
            $champion->addVersion($this);
        }

        return $this;
    }

    public function removeChampion(Champion $champion): static
    {
        if ($this->champions->removeElement($champion)) {
            $champion->removeVersion($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, League>
     */
    public function getLeagues(): Collection
    {
        return $this->leagues;
    }

    public function addLeague(League $league): static
    {
        if (!$this->leagues->contains($league)) {
            $this->leagues->add($league);
            $league->setVersion($this);
        }

        return $this;
    }

    public function removeLeague(League $league): static
    {
        if ($this->leagues->removeElement($league)) {
            // set the owning side to null (unless already changed)
            if ($league->getVersion() === $this) {
                $league->setVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Skin>
     */
    public function getSkins(): Collection
    {
        return $this->skins;
    }

    public function addSkin(Skin $skin): static
    {
        if (!$this->skins->contains($skin)) {
            $this->skins->add($skin);
            $skin->addVersion($this);
        }

        return $this;
    }

    public function removeSkin(Skin $skin): static
    {
        if ($this->skins->removeElement($skin)) {
            $skin->removeVersion($this);
        }

        return $this;
    }
}
