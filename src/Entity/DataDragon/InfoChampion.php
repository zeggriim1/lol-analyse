<?php

namespace App\Entity\DataDragon;

use App\Entity\Traits\IdEntityTrait;
use App\Repository\DataDragon\InfoChampionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InfoChampionRepository::class)]
class InfoChampion
{
    use IdEntityTrait;

    #[ORM\Column(name: 'attack', type: Types::INTEGER, nullable: false)]
    private ?int $attack = null;

    #[ORM\Column(name: 'defense', type: Types::INTEGER, nullable: false)]
    private ?int $defense = null;

    #[ORM\Column(name: 'magic', type: Types::INTEGER, nullable: false)]
    private ?int $magic = null;

    #[ORM\Column(name: 'difficulty', type: Types::INTEGER, nullable: false)]
    private ?int $difficulty = null;

    #[ORM\OneToOne(inversedBy: 'infoChampion', cascade: ['persist', 'remove'])]
    private ?Champion $champion = null;

    public function getAttack(): ?int
    {
        return $this->attack;
    }

    public function setAttack(int $attack): static
    {
        $this->attack = $attack;

        return $this;
    }

    public function getDefense(): ?int
    {
        return $this->defense;
    }

    public function setDefense(int $defense): static
    {
        $this->defense = $defense;

        return $this;
    }

    public function getMagic(): ?int
    {
        return $this->magic;
    }

    public function setMagic(int $magic): static
    {
        $this->magic = $magic;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): static
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getChampion(): ?Champion
    {
        return $this->champion;
    }

    public function setChampion(?Champion $champion): static
    {
        $this->champion = $champion;

        return $this;
    }
}
