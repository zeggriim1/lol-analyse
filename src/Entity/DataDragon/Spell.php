<?php

declare(strict_types=1);

namespace App\Entity\DataDragon;

use App\Entity\Cooldown;
use App\Entity\Leveltip;
use App\Entity\Range;
use App\Entity\SpellImage;
use App\Entity\Traits\IdEntityTrait;
use App\Repository\SpellRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: SpellRepository::class)]
class Spell
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(type: Types::STRING, length: 10, nullable: false)]
    private ?string $idLol = null;
    #[ORM\Column(type: Types::STRING, length: 150, nullable: false)]
    private ?string $name = null;
    #[ORM\Column(type: Types::TEXT, nullable: false)]
    private ?string $description = null;
    #[ORM\Column(type: Types::TEXT, nullable: false)]
    private ?string $tooltip = null;
    /**
     * @var Collection<int, SpellImage>
     */
    #[ORM\ManyToMany(targetEntity: SpellImage::class, mappedBy: 'entities', cascade: ['persist', 'remove'])]
    private Collection $images;
    #[ORM\OneToOne(inversedBy: 'spell', cascade: ['persist', 'remove'])]
    private ?Cooldown $cooldown = null;
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Leveltip $leveltip = null;
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Range $range = null;

    #[ORM\ManyToOne(inversedBy: 'spells')]
    private ?Champion $champion = null;

    public function __toString(): string
    {
        return sprintf('%s - %s', $this->id, $this->name);
    }

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function getIdLol(): ?string
    {
        return $this->idLol;
    }

    public function setIdLol(?string $idLol): self
    {
        $this->idLol = $idLol;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * @return Collection<int, SpellImage>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param Collection<int, SpellImage> $images
     */
    public function setImages(Collection $images): void
    {
        $this->images = $images;
    }

    public function addImage(?SpellImage $image): void
    {
        if (null === $image) {
            return;
        }

        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }

    public function removeImage(SpellImage $image): void
    {
        if (!$this->images->contains($image)) {
            $this->images->removeElement($image);
        }
    }

    public function getCooldown(): ?Cooldown
    {
        return $this->cooldown;
    }

    public function setCooldown(?Cooldown $cooldown): static
    {
        $this->cooldown = $cooldown;

        return $this;
    }

    public function getLeveltip(): ?Leveltip
    {
        return $this->leveltip;
    }

    public function setLeveltip(?Leveltip $leveltip): static
    {
        $this->leveltip = $leveltip;

        return $this;
    }

    public function getRange(): ?Range
    {
        return $this->range;
    }

    public function setRange(?Range $range): static
    {
        $this->range = $range;

        return $this;
    }

    public function getChampion(): ?Champion
    {
        return $this->champion;
    }

    public function setChampion(?Champion $champion): static
    {
        $this->champion = $champion;

        return $this;
    }
}
