<?php

namespace App\Entity\DataDragon;

use App\Entity\Traits\IdEntityTrait;
use App\Repository\SkinRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: SkinRepository::class)]
class Skin
{
    use IdEntityTrait;
    use TimestampableEntity;

    #[ORM\Column(type: Types::STRING, length: 10, nullable: false)]
    private ?string $idLol = null;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    private ?int $num = null;

    #[ORM\Column(type: Types::STRING, length: 50, nullable: false)]
    private ?string $name = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
    private ?bool $chromas = null;

    #[ORM\ManyToOne(targetEntity: Champion::class, inversedBy: 'skins')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Champion $champion = null;

    /**
     * @var Collection<int, Version>
     */
    #[ORM\ManyToMany(targetEntity: Version::class, inversedBy: 'skins')]
    private Collection $versions;

    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    public function getIdLol(): ?string
    {
        return $this->idLol;
    }

    public function setIdLol(string $idLol): static
    {
        $this->idLol = $idLol;

        return $this;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): static
    {
        $this->num = $num;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function isChromas(): ?bool
    {
        return $this->chromas;
    }

    public function setChromas(bool $chromas): static
    {
        $this->chromas = $chromas;

        return $this;
    }

    public function getChampion(): ?Champion
    {
        return $this->champion;
    }

    public function setChampion(?Champion $champion): static
    {
        $this->champion = $champion;

        return $this;
    }

    /**
     * @param Version[] $versions
     */
    public function setVersions(array $versions): void
    {
        foreach ($versions as $version) {
            $this->addVersion($version);
        }
    }

    public function addVersion(Version $version): static
    {
        if (!$this->versions->contains($version)) {
            $this->versions->add($version);
        }

        return $this;
    }

    /**
     * @return Collection<int, Version>
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function removeVersion(Version $version): static
    {
        $this->versions->removeElement($version);

        return $this;
    }
}
