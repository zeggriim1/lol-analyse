<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\DataDragon\Spell;
use App\Entity\Traits\IdEntityTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'spell_cooldown')]
class Cooldown
{
    use IdEntityTrait;

    #[ORM\Column(type: Types::INTEGER)]
    private int $value1;
    #[ORM\Column(type: Types::INTEGER)]
    private int $value2;
    #[ORM\Column(type: Types::INTEGER)]
    private int $value3;
    #[ORM\Column(type: Types::INTEGER)]
    private int $value4;
    #[ORM\Column(type: Types::INTEGER)]
    private int $value5;
    #[ORM\Column(type: Types::STRING)]
    private string $cooldownBurn;
    #[ORM\OneToOne(mappedBy: 'cooldown', cascade: ['persist', 'remove'])]
    private ?Spell $spell = null;

    public function getValue1(): int
    {
        return $this->value1;
    }

    public function setValue1(int $value1): self
    {
        $this->value1 = $value1;

        return $this;
    }

    public function getValue2(): int
    {
        return $this->value2;
    }

    public function setValue2(int $value2): self
    {
        $this->value2 = $value2;

        return $this;
    }

    public function getValue3(): int
    {
        return $this->value3;
    }

    public function setValue3(int $value3): self
    {
        $this->value3 = $value3;

        return $this;
    }

    public function getValue4(): int
    {
        return $this->value4;
    }

    public function setValue4(int $value4): self
    {
        $this->value4 = $value4;

        return $this;
    }

    public function getValue5(): int
    {
        return $this->value5;
    }

    public function setValue5(int $value5): self
    {
        $this->value5 = $value5;

        return $this;
    }

    public function getCooldownBurn(): string
    {
        return $this->cooldownBurn;
    }

    public function setCooldownBurn(string $cooldownBurn): self
    {
        $this->cooldownBurn = $cooldownBurn;

        return $this;
    }

    public function getSpell(): ?Spell
    {
        return $this->spell;
    }

    public function setSpell(?Spell $spell): static
    {
        // unset the owning side of the relation if necessary
        if (null === $spell && null !== $this->spell) {
            $this->spell->setCooldown(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $spell && $spell->getCooldown() !== $this) {
            $spell->setCooldown($this);
        }

        $this->spell = $spell;

        return $this;
    }
}
