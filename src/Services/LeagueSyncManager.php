<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\DataDragon\Version;
use App\Message\SynchronizeLeague;
use Symfony\Component\Messenger\MessageBusInterface;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

class LeagueSyncManager
{
    public function __construct(private readonly MessageBusInterface $messageBus) {}

    public function dispatchMessage(
        Version $versionId,
        ?Tier $tier = null,
        ?Queue $queue = null
    ): void {
        $tiers = null !== $tier ? [$tier] : [Tier::CHALLENGER, Tier::GRANDMASTER, Tier::MASTER];
        $queues = null !== $queue ? [$queue] : [Queue::RANKED_SOLO, Queue::RANKED_FLEX_SR];

        /** @var Tier $tier */
        foreach ($tiers as $tier) {
            /** @var Queue $queue */
            foreach ($queues as $queue) {
                $this->messageBus->dispatch(new SynchronizeLeague($versionId->getId(), $tier->value, $queue->value));
            }
        }
    }
}
