<?php

declare(strict_types=1);

namespace App\Services\Synchronizer;

use App\DataFixtures\Factory\LeagueFactory;
use App\Entity\DataDragon\Version;
use App\Repository\LeagueRepository;
use Doctrine\ORM\EntityManagerInterface;
use http\Exception\RuntimeException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zeggriim\RiotApiDataDragon\Endpoint\DataLeague\LeagueApiInterface;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

class LeagueSynchronizer
{
    public function __construct(
        private readonly LeagueRepository $leagueRepository,
        private readonly LeagueApiInterface $leagueApi,
        private readonly EntityManagerInterface $entityManager,
    ) {}

    public function synchronize(
        Version $version,
        Queue $queue,
        Tier $tier,
        ?SymfonyStyle $io = null
    ): void {
        $leagueApi = $this->getDataLeagueApi($queue, $tier);

        if (null === $leagueApi) {
            throw new RuntimeException('League API not found');
        }

        $league = $this->leagueRepository->findOneBy(['leagueIdLol' => $leagueApi['leagueId']]);

        if (null === $league) {
            $league = LeagueFactory::newLeague(
                $leagueApi['name'],
                $leagueApi['leagueId'],
                $queue,
                $tier,
                $version
            );

            $this->entityManager->persist($league);
            $this->entityManager->flush();

            $io?->success(
                sprintf(
                    'La League %s a été ajouté sur la Version %s',
                    $league->getLeagueIdLol(),
                    $version->getVersion()
                )
            );
        }

        $io?->info(
            sprintf(
                'La League %s éxiste déjà sur la Version %s',
                $league->getLeagueIdLol(),
                $version->getVersion()
            )
        );
    }

    /**
     * @return array<mixed>
     */
    private function getDataLeagueApi(Queue $queue, Tier $tier): ?array
    {
        return match ($tier) {
            Tier::CHALLENGER => $this->leagueApi->getChallenger($queue),
            Tier::GRANDMASTER => $this->leagueApi->getGrandMaster($queue),
            Tier::MASTER => $this->leagueApi->getMaster($queue),
            default => null
        };
    }
}
