<?php

declare(strict_types=1);

namespace App\Services\Synchronizer;

use App\DataFixtures\Factory\ChampionFactory;
use App\DataFixtures\Factory\InfoChampionFactory;
use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\Version;
use App\Repository\ChampionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zeggriim\RiotApiDataDragon\Endpoint\DataDragon\ChampionApiInterface;

readonly class ChampionSynchronizer
{
    public function __construct(
        private ChampionApiInterface $championApi,
        private ChampionRepository $championRepository,
        private EntityManagerInterface $entityManager
    ) {}

    public function synchronize(Version $version, ?SymfonyStyle $io = null): void
    {
        $championsApi = $this->championApi->getChampions($version->getVersion());

        $io?->progressStart(\count($championsApi));

        $countAdd = 0;

        foreach ($championsApi['data'] as $championApi) {
            $champion = $this->championRepository->findOneBy(['idLol' => $championApi['id']]);

            $io?->progressAdvance();

            if (null === $champion) {
                $countAdd++;
                $this->createChampion($championApi, $version);
            } else {
                $this->updateChampion($champion, $version);
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();

        $io?->progressFinish();
        $io?->success(
            sprintf(
                '%d ont été ajouté dans la table Champion avec la version %d',
                $countAdd,
                $version->getVersion()
            )
        );
    }

    /**
     * @template Info of array{'attack': int, 'defense': int, 'magic': int, 'difficulty': int}
     *
     * @param array{name: string, id: string, key: string, title: string, info: Info} $championApi
     */
    private function createChampion(array $championApi, Version $version): void
    {
        $infoChampion = InfoChampionFactory::newInfoChampion(
            $championApi['info']['attack'],
            $championApi['info']['defense'],
            $championApi['info']['magic'],
            $championApi['info']['difficulty'],
        );

        $champion = ChampionFactory::newChampion(
            name: $championApi['name'],
            idLol: $championApi['id'],
            key: $championApi['key'],
            title: $championApi['title'],
            version: [$version],
            infoChampion: $infoChampion
        );

        $this->entityManager->persist($champion);
    }

    private function updateChampion(Champion $champion, Version $version): void
    {
        $champion->setVersions([$version]);
    }
}
