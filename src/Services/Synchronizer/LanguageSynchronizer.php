<?php

declare(strict_types=1);

namespace App\Services\Synchronizer;

use App\Entity\DataDragon\Language;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zeggriim\RiotApiDataDragon\Endpoint\DataDragon\LanguageApiInterface;

readonly class LanguageSynchronizer
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LanguageApiInterface $languageApi
    ) {}

    public function synchronize(?SymfonyStyle $io = null): void
    {
        $languagesApi = $this->languageApi->getLanguages();
        $languages = $this->entityManager->getRepository(Language::class)->findAll();

        $io?->progressStart(\count($languagesApi));

        $countAdd = 0;
        foreach ($languagesApi as $languageApi) {
            $io?->progressAdvance();

            if (!$this->checkCode($languageApi, $languages)) {
                $countAdd++;
                $this->entityManager->persist($this->newLanguage($languageApi));
            }
        }

        $this->entityManager->flush();

        $io?->progressFinish();
        $io?->success(sprintf('%d ont été ajouté dans la table de Languages', $countAdd));
    }

    /**
     * @param Language[] $languages
     */
    private function checkCode(string $languageApi, array $languages): bool
    {
        foreach ($languages as $language) {
            if ($language->getCode() === $languageApi) {
                return true;
            }
        }

        return false;
    }

    private function newLanguage(string $languageApi): Language
    {
        $language = new Language();
        $language->setCode($languageApi);

        return $language;
    }
}
