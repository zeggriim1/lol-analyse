<?php

declare(strict_types=1);

namespace App\Services\Synchronizer;

use App\DataFixtures\Factory\SkinFactory;
use App\Entity\DataDragon\Champion;
use App\Entity\DataDragon\Version;
use App\Repository\ChampionRepository;
use App\Repository\SkinRepository;
use Doctrine\ORM\EntityManagerInterface;
use Zeggriim\RiotApiDataDragon\Endpoint\DataDragon\ChampionApiInterface;

readonly class SkinSynchronizer
{
    public function __construct(
        private ChampionApiInterface $championApi,
        private ChampionRepository $championRepository,
        private SkinRepository $skinRepository,
        private EntityManagerInterface $entityManager,
    ) {}

    public function synchronize(Champion $champion, Version $version): void
    {
        $champion = $this->championRepository->findChampionByIdAndVersion($champion->getId(), $version->getId());

        if (null !== $champion) {
            $championApi = $this->championApi->getChampion($champion->getIdLol(), $version->getVersion());

            if (\array_key_exists('data', $championApi)) {
                $championApi = $championApi['data'][$champion->getIdLol()];
            }

            foreach ($championApi['skins'] as $skinApi) {
                $skin = $this->skinRepository->findOneBy(['idLol' => $skinApi['id']]);

                if (null === $skin) {
                    $skin = SkinFactory::newSkin(
                        name: $skinApi['name'],
                        idLol: $skinApi['id'],
                        num: $skinApi['num'],
                        chromas: $skinApi['chromas'],
                    );
                }

                $skin->addVersion($version);
                $skin->setChampion($champion);
                $this->entityManager->persist($skin);
            }
        }

        $this->entityManager->flush();
    }
}
