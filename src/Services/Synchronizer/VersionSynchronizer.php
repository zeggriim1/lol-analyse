<?php

declare(strict_types=1);

namespace App\Services\Synchronizer;

use App\DataFixtures\Factory\VersionFactory;
use App\Entity\DataDragon\Version;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zeggriim\RiotApiDataDragon\Endpoint\DataDragon\VersionApiInterface;

readonly class VersionSynchronizer
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private VersionApiInterface $versionApi
    ) {}

    public function synchronize(?SymfonyStyle $io = null): void
    {
        $versionsApi = $this->versionApi->getVersions();
        $versions = $this->entityManager->getRepository(Version::class)->findAll();

        $io?->progressStart(\count($versionsApi));

        $countAdd = 0;
        foreach ($versionsApi as $versionApi) {
            $io?->progressAdvance();

            if (!$this->checkVersion($versionApi, $versions)) {
                $countAdd++;
                $this->entityManager->persist(VersionFactory::newVersion($versionApi));
            }
        }

        $this->entityManager->flush();
        $io?->progressFinish();
        $io?->success(sprintf('%d ont été ajouté dans la table de Version', $countAdd));
    }

    /**
     * @param Version[] $versions
     */
    private function checkVersion(string $versionApi, array $versions): bool
    {
        foreach ($versions as $version) {
            if ($version->getVersion() === $versionApi) {
                return true;
            }
        }

        return false;
    }
}
