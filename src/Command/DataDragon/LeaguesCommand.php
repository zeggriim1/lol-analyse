<?php

namespace App\Command\DataDragon;

use App\Repository\VersionRepository;
use App\Services\Synchronizer\LeagueSynchronizer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zeggriim\RiotApiDataDragon\Enum\Queue;
use Zeggriim\RiotApiDataDragon\Enum\Tier;

#[AsCommand(
    name: 'app:datadragon:leagues',
    description: 'Récupère les Leagues disponibles par Riot Games',
)]
class LeaguesCommand extends Command
{
    public function __construct(
        private readonly VersionRepository $versionRepository,
        private readonly LeagueSynchronizer $leagueSynchronizer
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('queue', InputArgument::REQUIRED)
            ->addArgument('tier', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $queueArgument = (string) $input->getArgument('queue');
        $queue = Queue::tryFrom($queueArgument);

        if (null === $queue) {
            $io->error('Le queue ' . $queueArgument . " n'existe pas.");

            return Command::FAILURE;
        }

        $tierArgument = (string) $input->getArgument('tier');
        $tier = Tier::tryFrom(strtoupper($tierArgument));

        if (null === $tier) {
            $io->error('Le tier ' . $tierArgument . " n'existe pas.");

            return Command::FAILURE;
        }

        $version = $this->versionRepository->findOneBy(['active' => true]);

        $this->leagueSynchronizer->synchronize($version, $queue, $tier, $io);

        return Command::SUCCESS;
    }
}
