<?php

namespace App\Command\DataDragon;

use App\Services\Synchronizer\LanguageSynchronizer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:datadragon:languages',
    description: 'Récupère les languages disponibles par Riot Games',
)]
class LanguagesCommand extends Command
{
    public function __construct(
        private readonly LanguageSynchronizer $languageSynchronizer
    ) {
        parent::__construct();
    }

    protected function configure(): void {}

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->languageSynchronizer->synchronize($io);

        return Command::SUCCESS;
    }
}
