<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionHasNoFlashBagException extends \RuntimeException
{
    public function __construct(
        SessionInterface $session,
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        $message = sprintf(
            'The session objet should be an instance of "%s" ("%s"given)',
            Session::class,
            $session::class
        );

        parent::__construct($message, $code, $previous);
    }
}
