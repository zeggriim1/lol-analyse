<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240808140111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création de la table Image avec le lien entre ChampionImage et SpellImage';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, full VARCHAR(100) NOT NULL, sprite VARCHAR(100) NOT NULL, category VARCHAR(100) NOT NULL, x INT NOT NULL, y INT NOT NULL, w INT NOT NULL, h INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spell_images (spell_image_id INT NOT NULL, spell_id INT NOT NULL, INDEX IDX_58BE497F29C91C67 (spell_image_id), INDEX IDX_58BE497F479EC90D (spell_id), PRIMARY KEY(spell_image_id, spell_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE champion_images (champion_image_id INT NOT NULL, champion_id INT NOT NULL, INDEX IDX_4FA7D47D95418605 (champion_image_id), INDEX IDX_4FA7D47DFA7FD7EB (champion_id), PRIMARY KEY(champion_image_id, champion_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spell (id INT AUTO_INCREMENT NOT NULL, id_lol VARCHAR(10) NOT NULL, name VARCHAR(150) NOT NULL, description LONGTEXT NOT NULL, tooltip LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE spell_images ADD CONSTRAINT FK_58BE497F29C91C67 FOREIGN KEY (spell_image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE spell_images ADD CONSTRAINT FK_58BE497F479EC90D FOREIGN KEY (spell_id) REFERENCES spell (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE champion_images ADD CONSTRAINT FK_4FA7D47D95418605 FOREIGN KEY (champion_image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE champion_images ADD CONSTRAINT FK_4FA7D47DFA7FD7EB FOREIGN KEY (champion_id) REFERENCES champion (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE messenger_messages');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, headers LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, queue_name VARCHAR(190) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), INDEX IDX_75EA56E0FB7336F0 (queue_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE spell_images DROP FOREIGN KEY FK_58BE497F29C91C67');
        $this->addSql('ALTER TABLE spell_images DROP FOREIGN KEY FK_58BE497F479EC90D');
        $this->addSql('ALTER TABLE champion_images DROP FOREIGN KEY FK_4FA7D47D95418605');
        $this->addSql('ALTER TABLE champion_images DROP FOREIGN KEY FK_4FA7D47DFA7FD7EB');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE spell_images');
        $this->addSql('DROP TABLE champion_images');
        $this->addSql('DROP TABLE spell');
    }
}
