<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240620110929 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add League';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE league (id INT AUTO_INCREMENT NOT NULL, version_id INT DEFAULT NULL, tier VARCHAR(50) NOT NULL, league_id_lol VARCHAR(255) NOT NULL, queue VARCHAR(40) NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_3EB4C3184BBC2705 (version_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE league ADD CONSTRAINT FK_3EB4C3184BBC2705 FOREIGN KEY (version_id) REFERENCES version (id)');
        $this->addSql('ALTER TABLE version ADD active TINYINT(1) DEFAULT 0 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE league DROP FOREIGN KEY FK_3EB4C3184BBC2705');
        $this->addSql('DROP TABLE league');
        $this->addSql('ALTER TABLE version DROP active');
    }
}
