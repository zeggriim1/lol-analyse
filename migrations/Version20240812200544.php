<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240812200544 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Lien entre Champion et Spell';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE spell ADD champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE spell ADD CONSTRAINT FK_D03FCD8DFA7FD7EB FOREIGN KEY (champion_id) REFERENCES champion (id)');
        $this->addSql('CREATE INDEX IDX_D03FCD8DFA7FD7EB ON spell (champion_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE spell DROP FOREIGN KEY FK_D03FCD8DFA7FD7EB');
        $this->addSql('DROP INDEX IDX_D03FCD8DFA7FD7EB ON spell');
        $this->addSql('ALTER TABLE spell DROP champion_id');
    }
}
