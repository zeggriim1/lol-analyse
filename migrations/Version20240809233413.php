<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240809233413 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création de la table Spell avec les liens Cooldown/Leveltip/Range';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE spell_cooldown (id INT AUTO_INCREMENT NOT NULL, value1 INT NOT NULL, value2 INT NOT NULL, value3 INT NOT NULL, value4 INT NOT NULL, value5 INT NOT NULL, cooldown_burn VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spell_leveltip (id INT AUTO_INCREMENT NOT NULL, label0 VARCHAR(150) NOT NULL, label1 VARCHAR(150) NOT NULL, label2 VARCHAR(150) NOT NULL, effect0 VARCHAR(255) NOT NULL, effect1 VARCHAR(255) NOT NULL, effect2 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `spell_range` (id INT AUTO_INCREMENT NOT NULL, value0 INT NOT NULL, value1 INT NOT NULL, value2 INT NOT NULL, value3 INT NOT NULL, value4 INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE spell ADD cooldown_id INT DEFAULT NULL, ADD leveltip_id INT DEFAULT NULL, ADD range_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE spell ADD CONSTRAINT FK_D03FCD8D993901D FOREIGN KEY (cooldown_id) REFERENCES spell_cooldown (id)');
        $this->addSql('ALTER TABLE spell ADD CONSTRAINT FK_D03FCD8DA78DAE63 FOREIGN KEY (leveltip_id) REFERENCES spell_leveltip (id)');
        $this->addSql('ALTER TABLE spell ADD CONSTRAINT FK_D03FCD8D2A82D0B1 FOREIGN KEY (range_id) REFERENCES `spell_range` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D03FCD8D993901D ON spell (cooldown_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D03FCD8DA78DAE63 ON spell (leveltip_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D03FCD8D2A82D0B1 ON spell (range_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE spell DROP FOREIGN KEY FK_D03FCD8D993901D');
        $this->addSql('ALTER TABLE spell DROP FOREIGN KEY FK_D03FCD8DA78DAE63');
        $this->addSql('ALTER TABLE spell DROP FOREIGN KEY FK_D03FCD8D2A82D0B1');
        $this->addSql('DROP TABLE spell_cooldown');
        $this->addSql('DROP TABLE spell_leveltip');
        $this->addSql('DROP TABLE `spell_range`');
        $this->addSql('DROP INDEX UNIQ_D03FCD8D993901D ON spell');
        $this->addSql('DROP INDEX UNIQ_D03FCD8DA78DAE63 ON spell');
        $this->addSql('DROP INDEX UNIQ_D03FCD8D2A82D0B1 ON spell');
        $this->addSql('ALTER TABLE spell DROP cooldown_id, DROP leveltip_id, DROP range_id');
    }
}
