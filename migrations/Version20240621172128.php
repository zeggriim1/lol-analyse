<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240621172128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE skin_version (skin_id INT NOT NULL, version_id INT NOT NULL, INDEX IDX_4CD11C4CF404637F (skin_id), INDEX IDX_4CD11C4C4BBC2705 (version_id), PRIMARY KEY(skin_id, version_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE skin_version ADD CONSTRAINT FK_4CD11C4CF404637F FOREIGN KEY (skin_id) REFERENCES skin (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE skin_version ADD CONSTRAINT FK_4CD11C4C4BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE skin CHANGE champion_id champion_id INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE skin_version DROP FOREIGN KEY FK_4CD11C4CF404637F');
        $this->addSql('ALTER TABLE skin_version DROP FOREIGN KEY FK_4CD11C4C4BBC2705');
        $this->addSql('DROP TABLE skin_version');
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('ALTER TABLE skin CHANGE champion_id champion_id INT DEFAULT NULL');
    }
}
