<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231209115709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Version Initialisation data';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE version (id INT AUTO_INCREMENT NOT NULL, version VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_BF1CD3C3BF1CD3C3 (version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE version');
    }
}
