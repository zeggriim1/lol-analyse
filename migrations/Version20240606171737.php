<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240606171737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add champion';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE champion (id INT AUTO_INCREMENT NOT NULL, id_lol VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, key_lol VARCHAR(255) NOT NULL, title VARCHAR(255) DEFAULT NULL, blurb LONGTEXT DEFAULT NULL, lore LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE champion_version (champion_id INT NOT NULL, version_id INT NOT NULL, INDEX IDX_3C60EE6EFA7FD7EB (champion_id), INDEX IDX_3C60EE6E4BBC2705 (version_id), PRIMARY KEY(champion_id, version_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE champion_version ADD CONSTRAINT FK_3C60EE6EFA7FD7EB FOREIGN KEY (champion_id) REFERENCES champion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE champion_version ADD CONSTRAINT FK_3C60EE6E4BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE champion_version DROP FOREIGN KEY FK_3C60EE6EFA7FD7EB');
        $this->addSql('ALTER TABLE champion_version DROP FOREIGN KEY FK_3C60EE6E4BBC2705');
        $this->addSql('DROP TABLE champion');
        $this->addSql('DROP TABLE champion_version');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
