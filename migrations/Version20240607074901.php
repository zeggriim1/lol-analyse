<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240607074901 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add info champion for version';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE info_champion (id INT AUTO_INCREMENT NOT NULL, champion_id INT DEFAULT NULL, attack INT NOT NULL, defense INT NOT NULL, magic INT NOT NULL, difficulty INT NOT NULL, UNIQUE INDEX UNIQ_527605DEFA7FD7EB (champion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE info_champion ADD CONSTRAINT FK_527605DEFA7FD7EB FOREIGN KEY (champion_id) REFERENCES champion (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE info_champion DROP FOREIGN KEY FK_527605DEFA7FD7EB');
        $this->addSql('DROP TABLE info_champion');
    }
}
