# Lol Analyse
<hr>

## Description

Un autre type de site web d'analyse de match pour League of Legends pourrait se concentrer sur l'analyse de la stratégie et des tactiques utilisées par les équipes et les joueurs individuels. Il pourrait offrir les fonctionnalités suivantes:

1. Analyse de la stratégie: une analyse détaillée des choix de champions, des objets, des runes et des talents utilisés par les joueurs, ainsi que des tactiques employées par les équipes pour remporter des victoires.
2. Étude de cas de matchs: une analyse détaillée de matchs clés, avec des commentaires sur les décisions prises par les joueurs et les équipes et leur impact sur le déroulement du match.
3. Comparaison des joueurs et des équipes: une fonctionnalité permettant aux utilisateurs de comparer les performances de différents joueurs et équipes, ainsi que leur style de jeu.
4. Classement des équipes et des joueurs par rapport à la stratégie utilisée.
5. Conseils et guides pour les stratégies et les tactiques: une section contenant des conseils et des guides pour améliorer sa performance en utilisant des stratégies efficaces.
6. Forum de discussion: une section où les utilisateurs peuvent discuter de la stratégie et des tactiques utilisées dans les matchs de League of Legends.
7. Intégration API de Riot Games pour récupérer les informations des parties et des joueurs.

Ce type de site web d'analyse de match pourrait être particulièrement utile pour les joueurs de haut niveau et les équipes compétitives, qui cherchent à améliorer leur performance en étudiant les tactiques et les stratégies utilisées par les meilleurs joueurs et équipes.

## Lancer l'environnement de dev

`make docker-up`



## Developpement

Site local: http://localhost:81
PhpMyAdmin: http://localhost:8085/
RabbitMQ: http://localhost:15672/
