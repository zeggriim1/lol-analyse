<?php


$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ;

$config = new PhpCsFixer\Config();

return $config
    ->setRiskyAllowed(true)
    ->setRules([
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@PSR2' => true,
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'binary_operator_spaces'=> false,
        'phpdoc_separation' => ['skip_unlisted_annotations' => true],
        'nullable_type_declaration_for_default_null_value' => true
    ])
    ->setFinder($finder)
    ;