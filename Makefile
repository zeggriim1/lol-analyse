#---Symfony-And-Docker-Makefile---------------#
# Author: https://github.com/yoanbernabeu
# License: MIT
#---------------------------------------------#

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_EXEC = $(DOCKER) exec
DOCKER_COMPOSE = docker-compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
DOCKER_PHP_RUN = $(DOCKER_EXEC) lol-analyse-php make qa-security-checker
#------------#

#---SYMFONY--#
SYMFONY = symfony
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
#------------#

#---COMPOSER-#
COMPOSER = composer
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
#------------#

#---NPM-----#
NPM = npm
NPM_INSTALL = $(NPM) install --force
NPM_UPDATE = $(NPM) update
NPM_BUILD = $(NPM) run build
NPM_DEV = $(NPM) run dev
NPM_WATCH = $(NPM) run watch
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.2-debian
PHPQA_RUN = $(DOCKER_RUN) --init -it --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#
#---------------------------------------------#

## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

## === 🐋  DOCKER ================================================
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)
.PHONY: docker-up

docker-build-up: ## Build les images et start les containers..
	$(DOCKER_COMPOSE) up --build -d
.PHONY: docker-build-up

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)
.PHONY: docker-stop

docker-ps: ## Liste docker containers.
	$(DOCKER_COMPOSE) ps
.PHONY: docker-ps
#---------------------------------------------#


## === 🐛  PHPQA =================================================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) make cmd-cs-fixer-dry-run
.PHONY: qa-cs-fixer-dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) make cmd-cs-fixer
.PHONY: qa-cs-fixer

qa-phpcs: ## Run phpcs.
	$(PHPQA_RUN) make cmd-phpcs
.PHONY: qa-phpcs

qa-phpmd: ## Run phpmd.
	$(PHPQA_RUN) make cmd-phpmd
.PHONY: qa-phpmd

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) make cmd-phpstan
.PHONY: qa-phpstan

qa-security-checker: ## Run security-checker.
	$(SYMFONY) security:check
.PHONY: qa-security-checker

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) make cmd-phpcpq
.PHONY: qa-phpcpd

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) make cmd-php-metrics
.PHONY: qa-php-metrics

lint-twigs: ## Lint twig files.
	make cmd-lint-twigs
.PHONY: qa-lint-twigs

lint-yaml: ## Lint yaml files.
	make cmd-lint-yaml
.PHONY: qa-lint-yaml

lint-container: ## Lint container.
	make cmd-lint-container
.PHONY: qa-lint-container

qa-lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction
.PHONY: qa-lint-schema

qa-audit: ## Run composer audit.
	$(COMPOSER) audit
.PHONY: qa-audit
#---------------------------------------------#

## ===  CMD =================================================
cmd-cs-fixer: ## Commande php-cs-fixer
	php-cs-fixer fix --config=.php-cs-fixer.php --allow-risky=yes --verbose
.PHONY: cmd-cs-fixer

cmd-cs-fixer-dry-run: ## Commande php-cs-fixer in dry-run mode
	php-cs-fixer fix --config=.php-cs-fixer.php --dry-run --diff --stop-on-violation --allow-risky=yes -vvv
.PHONY: cmd-cs-fixer-dry-run

cmd-phpcs: ## Commande phpcs
	phpcs --standard=phpcs.xml
.PHONY: cmd-phpcs

cmd-phpmd: ## Commande Phpmd
	phpmd src/ ansi phpmd.xml -vvv
.PHONY: cmd-phpmd

cmd-lint-container: ## Commande linter container
	$(SYMFONY_CONSOLE) lint:container
.PHONY: cmd-lint-container

cmd-lint-twigs: ## Commande linter twig file
	$(SYMFONY_CONSOLE) lint:twig ./templates
.PHONY: cmd-lint-twigs

cmd-lint-yaml: ## Commande linter twig yaml
	$(SYMFONY_CONSOLE) lint:yaml ./config
.PHONY: cmd-lint-yaml

cmd-phpcpd: ## Commande phpcpd (copy/paste detector).
	phpcpd ./src
.PHONY: cmd-phpcpd

cmd-php-metrics: ## Commande php-metrics.
	phpmetrics --report-html=var/phpmetrics ./src
.PHONY: cmd-php-metrics

cmd-phpstan: ## Commande phpstan.
	phpstan analyse ./src --level=7
.PHONY: cmd-phpstan
#---------------------------------------------#

## === ⭐  Symfony =================================================
sf-ddc: ## Doctrine Create Database
	symfony console doctrine:database:create --if-not-exists $(if $(ENV),--env=$(ENV))
.PHONY: sf-ddc

sf-ddd: ## Doctrine Drop Database
	symfony console doctrine:database:drop -f --if-exists $(if $(ENV),--env=$(ENV))
.PHONY: sf-ddd

sf-dfl: ## Load Fixtures
	symfony console doctrine:fixtures:load --purge-with-truncate --purger=mysql_purger -n
.PHONY: sf-dfl

sf-dmm: ## Doctrine Migration Migrate
	symfony console doctrine:migration:migrate --query-time --no-interaction $(if $(ENV), --env=$(ENV))
.PHONY: sf-dmm

sf-dsv: ## Doctrine Validate Schema
	symfony console doctrine:schema:validate
.PHONY: sf-dsv

sf-mm: ## Create make migration
	symfony console make:migration $(if $(ENV),--env=$(ENV))
.PHONY: sf-mm
#---------------------------------------------#

## === ⭐  Test =================================================
test: ## Run test PHPUNIT
	./bin/phpunit
.PHONY: test
#---------------------------------------------#

## === ⭐  OTHERS =================================================
before-commit: qa-cs-fixer qa-phpstan qa-phpmd qa-phpcs ## Run before commit. tests
.PHONY: before-commit

composer-install: ## Composer install
	composer install
.PHONY: composer

database: sf-ddd sf-ddc sf-dmm ## Reset database
.PHONY: database

database-fixtures: sf-ddd sf-ddc sf-dmm sf-dfl ## Reset database + load fixtures
.PHONY: database-fixtures

install: composer-install database-fixtures ## Install le projet en complet
.PHONY: install

start: docker-up ## Start project.A
.PHONY: start

stop: docker-stop ## Stop project
.PHONY: stop