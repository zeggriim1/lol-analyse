<?php

namespace Services\Riot\DataDragon;

use App\Services\Riot\DataDragon\ApiDataDragon;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DataDragonTest extends KernelTestCase
{
    private ApiDataDragon $datadragon;

    protected function setUp(): void
    {
        $container = self::getContainer();
        $this->datadragon = $container->get(ApiDataDragon::class);
    }

    public function testGetLanguages(): void
    {
        $languages = $this->datadragon->getLanguages();
        self::assertIsArray($languages);
    }

    public function testGetVersions(): void
    {
        $versions = $this->datadragon->getLanguages();
        self::assertIsArray($versions);
    }
}
